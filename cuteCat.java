public class cuteCat
{
	public double weight;
	public String name;
	public int longevity;
	
	public void sayHi()
	{
		System.out.println("I am a " + name + "!");
	}
	
	public void iLive()
	{
		System.out.println("I can live up to " + longevity + " year(s)!");
	}
	
}